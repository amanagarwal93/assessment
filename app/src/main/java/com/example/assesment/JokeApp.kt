package com.example.assesment

import android.app.Application
import com.example.assesment.di.apiModule
import com.example.assesment.di.networkModule
import com.example.assesment.di.repositoryModule
import com.example.assesment.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.logger.Level

class JokeApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@JokeApp)
            androidLogger(Level.DEBUG)
            modules(listOf(networkModule, apiModule, viewModelModule, repositoryModule))
        }
    }
}