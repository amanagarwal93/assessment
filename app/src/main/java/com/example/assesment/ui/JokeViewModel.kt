package com.example.assesment.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.assesment.data.JokeRepository
import com.example.assesment.data.utils.Result
import kotlinx.coroutines.launch

class JokeViewModel(private val jokeRepository: JokeRepository) : ViewModel() {

    private val joke = MutableLiveData<Result<String?>>()
    val getJoke = joke

    init {
        fetchJoke()
    }

    fun fetchJoke() {
        viewModelScope.launch {
            jokeRepository.fetchJoke().collect {
                joke.value = it
            }
        }
    }
}