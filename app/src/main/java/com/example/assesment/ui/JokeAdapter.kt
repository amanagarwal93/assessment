package com.example.assesment.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assesment.databinding.JokeItemBinding

class JokeAdapter(private val jokeList: List<String>) :
    RecyclerView.Adapter<JokeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = JokeItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        jokeList[position].let {
            holder.setJoke(it)
        }
    }

    override fun getItemCount() = jokeList.size

    class ViewHolder(private val binding: JokeItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setJoke(joke: String) {
            binding.jokeTextView.text = joke
        }
    }
}