package com.example.assesment.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assesment.data.utils.Result
import com.example.assesment.databinding.ActivityJokeBinding
import kotlinx.coroutines.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.coroutines.CoroutineContext

class JokeActivity : AppCompatActivity(), CoroutineScope{

    private val jokeViewModel by viewModel<JokeViewModel>()
    private lateinit var binding: ActivityJokeBinding
    private var jokeList = ArrayList<String>()
    private lateinit var repeatJob: Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJokeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.apply {
            jokeRecyclerView.layoutManager = layoutManager
            jokeRecyclerView.setHasFixedSize(true)
            jokeRecyclerView.adapter = JokeAdapter(jokeList)
        }
    }

    override fun onStart() {
        super.onStart()
        jokeViewModel.fetchJoke()
    }

    private fun repeatFun(): Job {
        return launch {
            while(isActive) {
                delay(60*1000)
                observeJokes()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        repeatJob = repeatFun()
    }

    private fun observeJokes() {
        jokeViewModel.getJoke.observe(this) {
            when (it?.status) {
                Result.Status.SUCCESS -> it.data?.let {
                    when {
                        jokeList.size != 10 -> jokeList.add(it)
                        else -> {
                            jokeList.remove(jokeList[0])
                            jokeList.add(it)
                        }
                    }
                    binding.jokeRecyclerView.adapter?.notifyDataSetChanged()
                }
                Result.Status.ERROR -> {

                }
                else -> {
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        repeatJob.cancel()
    }

    override val coroutineContext: CoroutineContext
        get() = Job() + Dispatchers.Main
}