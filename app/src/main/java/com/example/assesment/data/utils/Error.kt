package com.example.assesment.data.utils

data class Error(
    val status_code: Int = 0,
    val status_message: String? = null
)