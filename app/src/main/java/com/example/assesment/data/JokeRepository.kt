package com.example.assesment.data

import com.example.assesment.data.remote.JokeService
import com.example.assesment.data.utils.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Response

class JokeRepository(private val apiService: JokeService) {

    suspend fun fetchJoke(): Flow<Result<String?>?> {
        return flow {
            emit(Result.loading())
            val result = getResponse(request = { apiService.getJoke() })
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    // get response
    private suspend fun <T> getResponse(
        request: suspend () -> Response<T>
    ): Result<T?> {
        return try {
            val result = request.invoke()
            when {
                result.isSuccessful -> return Result.success(result.body())
                else -> Result.error(result.errorBody().toString(), null)
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            Result.error("Unknown Error", null)
        }
    }

}